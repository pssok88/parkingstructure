﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace parkingservice.enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ParkingSpaceTypes
    {
        Unknown = 0,
        General,
        Valet,
        FrequentFlyer
    }
}
