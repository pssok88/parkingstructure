﻿using parkingservice.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace parkingservice.dto
{
    public partial class ValetParkingSpot : BaseParkingSpot
    {
        private const ParkingSpaceTypes TYPE = ParkingSpaceTypes.Valet;
        private const double HOURLY_FEE = 6.0;

        public ValetParkingSpot(DateTime _start) : base(_start)
        { }
        public override double HourlyFee => HOURLY_FEE;

        public override ParkingSpaceTypes Type => TYPE;


    }
}
