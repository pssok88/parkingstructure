﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace parkingservice.dto
{
    public sealed class AutomobileOwner
    {
        public string Name { get; set;  }
    }
}
