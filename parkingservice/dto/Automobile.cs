﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace parkingservice.dto
{
    public class Automobile
    {
        private string LicensePlate { get; set; }

        private AutomobileOwner _owner;
        private  AutomobileOwner Owner { get { return _owner; } }

        public Automobile(string name) {
            this._owner = new AutomobileOwner() { Name = name };
        }
    }
}
