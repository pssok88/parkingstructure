﻿using parkingservice.enums;
using System;

namespace parkingservice.dto
{
    public abstract class BaseParkingSpot
    {
        public abstract double HourlyFee
        {
            get;
        }

        private DateTime _start;
        public DateTime Start
        {
            get
            {
                return _start;
            }
        }

        public abstract ParkingSpaceTypes Type
        {
            get;
        }

        public BaseParkingSpot(DateTime start)
        {
            this._start = start;
        }

        public virtual string PrettyType => Type.ToString();
    }
}
