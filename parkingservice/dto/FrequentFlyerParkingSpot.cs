﻿using parkingservice.enums;
using System;

namespace parkingservice.dto
{
    public partial class FrequentFlyerParkingSpot : BaseParkingSpot
    {
        private const ParkingSpaceTypes TYPE = ParkingSpaceTypes.FrequentFlyer;
        private const double HOURLY_FEE = 12.0;

        public FrequentFlyerParkingSpot(DateTime _start) : base(_start)
        { }

        public override double HourlyFee => HOURLY_FEE;

        public override ParkingSpaceTypes Type => TYPE;
    }
}
