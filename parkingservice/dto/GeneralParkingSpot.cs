﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using parkingservice.enums;

namespace parkingservice.dto
{
    public partial class GeneralParkingSpot : BaseParkingSpot
    {
        private const ParkingSpaceTypes TYPE = ParkingSpaceTypes.General;
        private const double HOURLY_FEE = 3.0;

        public GeneralParkingSpot(DateTime _start) : base(_start)
        { }

        public override double HourlyFee => HOURLY_FEE;

        public override ParkingSpaceTypes Type => TYPE;
    }
}
